//C0721763 - Akhil Somaraj

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class BlazeDemoTest {

	WebDriver driver;

	@Before
	public void setUp() throws Exception {

		System.setProperty("webdriver.chrome.driver", "/Users/akhilsraj/Desktop/Chrome/chromedriver");
		driver = new ChromeDriver();

		String baseUrl = "http://blazedemo.com/";

		driver.get(baseUrl);
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(10000);
		driver.close();
	}

	@Test
	public void departureCitiesTC1() {

		//List<WebElement> optionList = driver.findElements(By.name("fromPort"));
		Select cityListDropDown = new Select(driver.findElement(By.name("fromPort")));
		List<WebElement> cityList  = cityListDropDown.getOptions();
		assertEquals(7, cityList);
	}

	@Test
	public void flightDetailsTC2() {
		
		WebElement fromCityDropDown = driver.findElement(By.name("fromPort"));
		Select formCity = new Select(fromCityDropDown);  
		formCity.selectByValue("Paris");

		WebElement toCityDropDown = driver.findElement(By.name("toPort"));
		Select toCity = new Select(toCityDropDown);  
		toCity.selectByValue("London");

		WebElement subscribeButton = driver.findElement(By.cssSelector(".container .btn-primary"));
		subscribeButton.click();

		WebElement flightTable = driver.findElement(By.className("table"));

		List<WebElement> rows_table = flightTable.findElements(By.tagName("tr"));

		int rows_count = rows_table.size();

		for (int row = 0; row < rows_count; row++) {

			List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));

			int columns_count = Columns_row.size();

			for (int column = 0; column < columns_count; column++) {
				String celltext = Columns_row.get(column).getText();

				if (celltext == "12") {
				
					System.out.println(celltext);
				}
			}
		}
	}

	@Test
	public void purchaseConfirmTC3() {
		
		WebElement fromCityDropDown = driver.findElement(By.name("fromPort"));
		Select formCity = new Select(fromCityDropDown);  
		formCity.selectByValue("Paris");

		WebElement toCityDropDown = driver.findElement(By.name("toPort"));
		Select toCity = new Select(toCityDropDown);  
		toCity.selectByValue("London");

		WebElement subscribeButton = driver.findElement(By.cssSelector(".container .btn-primary"));
		subscribeButton.click();

		WebElement chooseFlightBtn = driver.findElement(By.cssSelector(".btn-small"));
		subscribeButton.click();
	}
}