//C0721763 - Akhil Somaraj

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest {
	Snake snake1;
	Snake snake2;
	

	@Before
	public void setUp() throws Exception {
		snake1 = new Snake("Peter", 10, "coffee");
		snake2 = new Snake("Takis", 80, "vegetables");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void isHealthyTestCase1() {
		assertEquals(false, snake1.isHealthy());
		assertEquals(true, snake2.isHealthy());

	}

	@Test
	public void fitsIntheCageTestCase2() {
		int cageLength = 20;
		assertEquals(true, snake1.fitsInCage(cageLength));
		assertEquals(false, snake2.fitsInCage(cageLength));

	}
}
